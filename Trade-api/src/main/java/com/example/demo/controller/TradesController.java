package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//Call to entity class for access to database
import com.example.demo.entity.StockTrades;

//Import service class for interface implementation
import com.example.demo.service.StockTradeService;

@RestController
@RequestMapping("trades")
public class TradesController {

	//is used to inject a class which is not instantiated like service in this case
	@Autowired
	private StockTradeService service;
	
	@GetMapping(value = "/{ticker}")
	public StockTrades getTradeByTicker (@PathVariable("ticker") String ticker) {
		return service.getTradeByTicker(ticker);
	}
	
}
