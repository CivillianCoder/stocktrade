package com.example.demo.repository;

import java.sql.ResultSet;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
//Use JDBC Template for SQL queries
import org.springframework.jdbc.core.JdbcTemplate;

//For Row Mapping using ORM. Must match constructor class
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

//Import StockTrades Class from entity
import com.example.demo.entity.StockTrades;

@Repository
public class MySQLStockTradesRepository implements StockTradesRepository {

	//Create JDBC template
	@Autowired
	JdbcTemplate template;
	
//Create RowMapper
//Create SQL string and use queryForObject method
	
	@Override
	public StockTrades getTradeByTicker(String Ticker) {
		String sql = "SELECT TICKER, PRICE, DECISION FROM `stock-analysis-refactored` WHERE TICKER=?;";
		return template.queryForObject(sql, new TradeRowMapper(), Ticker);
	}

}

//TradeRowMapper must match Exactly to the parameter constructor

class TradeRowMapper implements RowMapper<StockTrades> {
	
	@Override
	public StockTrades mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new StockTrades(rs.getString("ticker"), rs.getFloat("price"), rs.getString("decision"));
	}
}


