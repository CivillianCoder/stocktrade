package com.example.demo.entity;

public class StockTrades {
	private String ticker;
	private Float price;
	private String decision;
	
	//Create Constructors
	
	public StockTrades(String ticker, Float price, String decision) {
		super();
		this.ticker = ticker;
		this.price = price;
		this.decision = decision;
	}
	
	public StockTrades() {
		
	}
	//  End Constructors
	
	//	Create getters and setters
		//TICKER
	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}
		//PRICE
	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
		//BUYorSELL
	public String getDecision() {
		return decision;
	}

	public void setDecision(String decision) {
		this.decision = decision;
	}	


		
}
