package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.StockTrades;
//Import interface for making calls to the function
import com.example.demo.repository.StockTradesRepository;

@Service
public class StockTradeService {
	
	@Autowired
	private StockTradesRepository repository;
	
	//get Trade by Ticker
	public StockTrades getTradeByTicker (String ticker) {
		return repository.getTradeByTicker(ticker);
	}
	
}
